from pyspark.sql import SparkSession
from pyspark.sql.functions import col, sum, regexp_extract, regexp_replace
from pyspark.sql.utils import AnalysisException
from pyspark.sql.types import IntegerType, DoubleType
import os


def remove_duplicates(df, columns_to_check):
    df_no_duplicates = df.dropDuplicates(columns_to_check)
    return df_no_duplicates


def fix_duplicates(df, columns_to_check, details=False):
    duplicate_counts = df.groupBy(columns_to_check).count()
    duplicate_rows = duplicate_counts.filter("count > 1")
    if duplicate_rows.count() > 0:
        total_duplicates = duplicate_rows.agg({'count': 'sum'}).collect()[
            0]['sum(count)'] - duplicate_rows.count()
        print(
            f"Found {total_duplicates} duplicates on column {columns_to_check}: Removal in progress")
        if (details == True):
            print(f"Duplicate rows based on columns {columns_to_check}:")
            duplicate_rows.show(truncate=False)
        return remove_duplicates(df, columns_to_check)
    else:
        print(f"No duplicate rows found on column {columns_to_check}.")
        return df


def has_null_values(df):
    total_null_count = df.select(
        [sum(col(c).isNull().cast("int")).alias(c) for c in df.columns]
    )
    if any(value > 0 for value in total_null_count.head(1)[0].asDict().values()):
        print("Null values found.")
        return True
    else:
        print("No null values found.")
        return False


def create_df_loyers():
    loyer_csv_path_maisons = "./dataset/loyer_maison_france_2023.csv"
    df_loyers_maisons = spark.read.csv(loyer_csv_path_maisons, header=True,
                                       inferSchema=True, sep=";")
    print("Checking duplicates for df_loyers...")
    df_loyers_maisons = fix_duplicates(df_loyers_maisons, ["INSEE_C"])
    df_loyers_maisons = df_loyers_maisons.withColumn("loypredm2", regexp_replace(
        col("loypredm2"), ",", ".").cast("double"))

    loyer_csv_path_apparts = "./dataset/loyer_appt_france_2023.csv"
    df_loyers_apparts = spark.read.csv(loyer_csv_path_apparts, header=True,
                                       inferSchema=True, sep=";")
    print("Checking duplicates for df_apparts...")
    df_loyers_apparts = fix_duplicates(df_loyers_apparts, ["INSEE_C"])
    df_loyers_apparts = df_loyers_apparts.withColumn("loypredm2", regexp_replace(
        col("loypredm2"), ",", ".").cast("double"))
    return (df_loyers_maisons, df_loyers_apparts)


def create_df_villes():
    villes_csv_path = "./dataset/cities.csv"
    df_villes = spark.read.csv(villes_csv_path, header=True,
                               inferSchema=True, sep=",")
    print("Checking duplicates for df_villes...")
    df_villes = fix_duplicates(df_villes, ["insee_code"])
    return (df_villes)


def create_df_revenus():
    revenus_csv_path = "./dataset/infos_carreau.csv"
    df_revenus = spark.read.csv(revenus_csv_path, header=True,
                                inferSchema=True, sep=";")
    df_revenus = df_revenus.drop("Code")
    df_revenus = df_revenus.withColumn(
        "Libellé", regexp_extract(df_revenus["Libellé"], r"(\d+)", 1))
    df_revenus = df_revenus.withColumnRenamed("Libellé", "INSEE_Code")

    df_revenus = (
        df_revenus.groupBy("INSEE_Code")
        .agg(
            sum("Population").alias("population"),
            (sum(col("MenagesPauvres") * col("Population")) /
             sum("Population")).alias("menages_pauvres"),
            (sum(col("NiveauDeVie") * col("Population")) /
             sum("Population")).alias("niveau_de_vie_moyen")
        )
    )
    df_revenus = df_revenus.withColumn("niveau_de_vie_moyen", col(
        "niveau_de_vie_moyen").cast(IntegerType()))
    df_revenus = df_revenus.drop("population")
    df_revenus = df_revenus.drop("menages_pauvres")
    return (df_revenus)


def create_df_population():
    population_csv_path = "./dataset/population_villes.csv"
    df_pop = spark.read.csv(population_csv_path, header=True,
                            inferSchema=True, sep=";")
    print("Checking duplicates for df_pop...")
    df_pop = fix_duplicates(df_pop, ["Code"])
    return (df_pop)


def generate_output_data(merged_df, df_name):
    output_json_path = f"./dataset/{df_name}_json"
    output_csv_path = f"./dataset/{df_name}_csv"
    merged_df.coalesce(1).write.mode("overwrite").json(output_json_path)
    merged_df.coalesce(1).write.mode(
        "overwrite").csv(output_csv_path, header=True)


def merge_and_generate(df_loyers, df_name):
    df_villes = create_df_villes()
    df_revenus = create_df_revenus()
    df_pop = create_df_population()
    merged_df = df_loyers.join(
        df_villes, df_loyers["INSEE_C"] == df_villes["insee_code"], "inner")
    merged_df = merged_df.join(
        df_revenus, merged_df["INSEE_C"] == df_revenus["INSEE_Code"])
    merged_df = merged_df.join(
        df_pop, merged_df["INSEE_C"] == df_pop["Code"])
    merged_df = merged_df.withColumnRenamed("lwr.IPm2", "lwr_IPm2")
    merged_df = merged_df.withColumnRenamed("upr.IPm2", "upr_IPm2")
    columns_to_drop = ["insee_code", "id_zone", "LIBGEO", "EPCI",
                       "DEP", "REG", "TYPPRED", "nbobs_com", "nbobs_mail",
                       "R2_adj", "label", "department_name", "department_number",
                       "region_name", "region_geojson_name", "zip_code",
                       "lwr_IPm2", "upr_IPm2", "Libellé"]
    merged_df = merged_df.drop(*columns_to_drop)

    # Check for duplicates and null values
    print("Checking duplicates for df_merged...")
    df_merged = fix_duplicates(merged_df, ["INSEE_C"])
    if has_null_values(merged_df):
        raise AnalysisException(
            "Dataframe contains null value, not allowed here")
    merged_df.describe().show()
    generate_output_data(merged_df, df_name)


def create_merged_data():
    df_loyers_maisons, df_loyers_apparts = create_df_loyers()
    merge_and_generate(df_loyers_maisons, "merged_dataset_maisons")
    merge_and_generate(df_loyers_apparts, "merged_dataset_apparts")


if __name__ == "__main__":
    os.environ["HADOOP_HOME"] = "/home/jules/TSP/CSC_Data/hadoop-3.3.6"
    hadoop_native_path = os.path.join(
        os.environ["HADOOP_HOME"], "lib", "native")
    os.environ["LD_LIBRARY_PATH"] = f"{hadoop_native_path}:{os.environ.get('LD_LIBRARY_PATH', '')}"

    spark = SparkSession.builder \
        .appName("projet-loyers") \
        .getOrCreate()
    create_merged_data()
