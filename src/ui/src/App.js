import React from 'react';
import UpgradeRentComponent from './components/UpgradeRentComponent';
import UpgradeLivingComponent from './components/UpgradeLivingComponent';
import RatioComponent from './components/RatioComponent';
import BestRatioComponent from './components/BestRatioComponent';
import NavBar from "./components/NavBar";
import { Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

import data_rent from "./data/data_rent.json";
import data_ratio_house from "./data/data_ratio_house.json";
import data_ratio_appartment from "./data/data_ratio_appartment.json";
import MapComponent from './components/MapComponent';

function App() {
  return (
    <>
      <NavBar />
      <Routes>
        <Route
          path="/rent"
          element={
            <UpgradeRentComponent points={data_rent} />
          }
        />
        <Route
          path="/living"
          element={
            <UpgradeLivingComponent points={data_rent} />
          }
        />
        <Route
          path="/ratio-house"
          element={
            <MapComponent title={"Rentability of Houses in France by City"} >
              <RatioComponent points={data_ratio_house} />
            </MapComponent>
          }
        />
        <Route
          path="/ratio-appartment"
          element={
            <MapComponent title={"Rentability of Appartments in France by City"} >
              <RatioComponent points={data_ratio_appartment} />
            </MapComponent>
          }
        />
        <Route
          path="/best-ratio-house"
          element={
            <MapComponent title={"TOP 1000 cities according to housing profitability"} >
              <BestRatioComponent points={data_ratio_house} />
            </MapComponent>
          }
        />
        <Route
          path="/best-ratio-appartment"
          element={
            <MapComponent title={"TOP 1000 cities according to appartment profitability"} >
              <BestRatioComponent points={data_ratio_appartment} />
            </MapComponent>
          }
        />
      </Routes>
    </>
  )
}

export default App;
