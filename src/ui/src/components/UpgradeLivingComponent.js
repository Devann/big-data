import React from 'react';
import Titre from "./Titre";
import LivingComponent from './LivingComponent';
import Stats from './Stats';
import living_stats from "../data/living_stats.json"

function UpgradeLivingComponent({ points }) {// Assurez-vous d'avoir vos données de location ici

    return (
        <div className="container-fluid">
            <div className="row">
                {/* Colonne de gauche pour les statistiques */}
                <div className="col-md-2 d-flex align-items-center justify-content-center">
                    <Stats data={living_stats} />
                </div>

                {/* Colonne de droite pour la carte */}
                <div className="col-md-10">
                    <Titre text={"Standard of Living in France by City"} />
                    <LivingComponent points={points} />
                </div>
            </div>
        </div>
    );
}

export default UpgradeLivingComponent;
