import React from 'react';
import Plot from 'react-plotly.js';

const RentComponent = ({ points }) => {

    const data = points;

    const customColorScale = [
        [0.0, 'rgb(12, 51, 131)'],   // Bleu très foncé
        [0.2, 'rgb(10, 136, 186)'],  // Bleu clair
        [0.4, 'rgb(242, 211, 56)'],  // Jaune
        [0.5, 'rgb(242, 143, 56)'],  // Orange clair
        [0.6, 'rgb(217, 30, 30)'],   // Rouge clair
        [0.7, 'rgb(91, 5, 5)'],      // Rouge foncé
        [0.8, 'rgb(32, 7, 0)'],      // Brun foncé
        [1.0, 'rgb(0, 0, 0)'],       // Noir
    ];

    // Use data to create a Plotly map
    const plotData = [
        {
            type: 'scattergeo',
            mode: 'markers',
            lon: data.map(city => city.longitude),
            lat: data.map(city => city.latitude),
            text: data.map(city => `City: ${city.city_code}<br>Residuals: ${city.residuals}<br>Population: ${city.Population}`),
            marker: {
                color: data.map(city => city.residuals),
                colorscale: customColorScale,
                opacity: 1,
                colorbar: {
                    title: 'Residuals',
                },
            },
        },
    ];

    const layout = {
        autosize: false,
        width: 1300,
        height: 600,
        geo: {
            center: { lon: 2.2, lat: 46.6 }, // Set the center to France's approximate coordinates
            projection: {
                scale: 19, // Adjust the scale for zoom level
            },
        },
    };

    return <Plot data={plotData} layout={layout} />;
};

export default RentComponent;
