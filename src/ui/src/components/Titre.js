// Bibliothèques
import React from "react";

/**
 * Le rôle de ce composant est d'afficher un titre centré sur la page.
 */
function Titre({ text }) {
  return (
    <div className="container d-flex justify-content-center">
      <h1>{text}</h1>
    </div>
  );
}

export default Titre;
