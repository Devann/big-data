import React from 'react';
import Plot from 'react-plotly.js';

const BestRatioComponent = ({ points }) => {

    const data = points.filter(city => city.Population > 1000).sort((a, b) => a.residuals - b.residuals).slice(0, 1000);

    // Use data to create a Plotly map
    const plotData = [
        {
            type: 'scattergeo',
            mode: 'markers',
            lon: data.map(city => city.longitude),
            lat: data.map(city => city.latitude),
            text: data.map(city => `City: ${city.city_code}<br>Residuals: ${city.residuals}<br>Population: ${city.Population}`),
            marker: {
                color: data.map(city => city.residuals),
                colorscale: "Portland",
                opacity: 1,
                colorbar: {
                    title: 'Residuals',
                },
            },
        },
    ];

    const layout = {
        autosize: false,
        width: 1300,
        height: 600,
        geo: {
            center: { lon: 2.2, lat: 46.6 }, // Set the center to France's approximate coordinates
            projection: {
                scale: 19, // Adjust the scale for zoom level
            },
        },
    };

    return <Plot data={plotData} layout={layout} />;
};

export default BestRatioComponent;
