import React from 'react';

function Stats({ data }) {
    return (
        < div className="text-center" >
            <h2>Statistics</h2>
            <p>Mean: {data.mean}</p>
            <p>Standard Deviation: {data.stddvt}</p>
            <p>Max: {data.max}</p>
            <p>Min: {data.min}</p>
        </div >
    )
}

export default Stats;