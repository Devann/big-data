import React from 'react';
import Titre from "./Titre";
import RentComponent from './RentComponent';
import Stats from './Stats';
import rent_stats from "../data/rent_stats.json"

function UpgradeRentComponent({ points }) {// Assurez-vous d'avoir vos données de location ici

    return (
        <div className="container-fluid">
            <div className="row">
                {/* Colonne de gauche pour les statistiques */}
                <div className="col-md-2 d-flex align-items-center justify-content-center">
                    <Stats data={rent_stats} />
                </div>

                {/* Colonne de droite pour la carte */}
                <div className="col-md-10">
                    <Titre text={"Rent Price in France by City"} />
                    <RentComponent points={points} />
                </div>
            </div>
        </div>
    );
}

export default UpgradeRentComponent;
