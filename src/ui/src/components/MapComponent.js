import React from 'react';
import Titre from "./Titre";

function MapComponent(props) {
    return (
        <main>
            <Titre text={props.title} />
            {props.children}
        </main>
    )
}

export default MapComponent;