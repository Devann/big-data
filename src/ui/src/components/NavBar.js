// Libraries
import React from "react";
import { NavLink } from "react-router-dom";

/**
 * The role of this component is to create a navigation bar.
 * The navigation bar uses the react-router-dom library to handle navigation
 * between the various pages of the application.
 */
function NavBar() {

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <ul className="navbar-nav">
                    <li className="nav-item display-7">
                        <NavLink className="nav-link font-weight-bold" to="/rent">
                            Rent
                        </NavLink>
                    </li>
                    <li className="nav-item display-7">
                        <NavLink className="nav-link font-weight-bold" to="/living">
                            Standard of living
                        </NavLink>
                    </li>
                    <li className="nav-item display-7">
                        <NavLink className="nav-link font-weight-bold" to="/ratio-house">
                            Ratio Houses
                        </NavLink>
                    </li>
                    <li className="nav-item display-7">
                        <NavLink className="nav-link font-weight-bold" to="/ratio-appartment">
                            Ratio Appartments
                        </NavLink>
                    </li>
                    <li className="nav-item display-7">
                        <NavLink className="nav-link font-weight-bold" to="/best-ratio-house">
                            TOP 1000 Ratio Houses
                        </NavLink>
                    </li>
                    <li className="nav-item display-7">
                        <NavLink className="nav-link font-weight-bold" to="/best-ratio-appartment">
                            TOP 1000 Ratio Appartments
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

// Exportation
export default NavBar;
