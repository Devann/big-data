import React from 'react';
import Plot from 'react-plotly.js';

const LivingComponent = ({ points }) => {

    const data = points;

    const customColorScale = [
        [0.0, 'rgb(173, 216, 230)'], // Bleu très clair
        [0.1, 'rgb(10, 136, 186)'],  // Bleu clair
        [0.2, 'rgb(242, 211, 56)'],  // Jaune
        [0.3, 'rgb(242, 143, 56)'],  // Orange clair
        [0.4, 'rgb(217, 30, 30)'],   // Rouge clair
        [0.5, 'rgb(91, 5, 5)'],      // Rouge foncé
        [0.6, 'rgb(32, 7, 0)'],      // Brun foncé
        [0.7, 'rgb(32, 7, 0)'],      // Brun foncé
        [0.8, 'rgb(32, 7, 0)'],      // Brun foncé
        [0.9, 'rgb(32, 7, 0)'],      // Brun foncé
        [1.0, 'rgb(0, 0, 0)'],       // Noir
    ];

    // Use data to create a Plotly map
    const plotData = [
        {
            type: 'scattergeo',
            mode: 'markers',
            lon: data.map(city => city.longitude),
            lat: data.map(city => city.latitude),
            text: data.map(city => `City: ${city.city_code}<br>Average Rent: ${city.loypredm2}<br>Lifestyle: ${city.niveau_de_vie_moyen}`),
            marker: {
                color: data.map(city => city.niveau_de_vie_moyen),
                colorscale: customColorScale,
                opacity: 1,
                colorbar: {
                    title: 'Average household revenue',
                },
            },
        },
    ];

    const layout = {
        autosize: false,
        width: 1300,
        height: 600,
        geo: {
            center: { lon: 2.2, lat: 46.6 }, // Set the center to France's approximate coordinates
            projection: {
                scale: 19, // Adjust the scale for zoom level
            },
        },
    };

    return <Plot data={plotData} layout={layout} />;
};

export default LivingComponent;
