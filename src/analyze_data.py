from pyspark.ml.feature import VectorAssembler
from pyspark.ml.clustering import KMeans
from pyspark.sql import SparkSession
import plotly.express as px
import os
from pyspark.sql.functions import col, expr
from pyspark.sql import SparkSession
from pyspark.ml.regression import LinearRegression


def plot_kmeans_clustering(df, x_col, y_col, city_code_col, k=3):
    """
    Perform K-means clustering on the provided DataFrame and plot the results.

    Parameters:
    - df: PySpark DataFrame
    - x_col: Name of the column for x-axis
    - y_col: Name of the column for y-axis
    - city_code_col: Name of the column for city codes (used in hover data)
    - k: Number of clusters for K-means

    Returns:
    - Plotly scatter plot
    """
    vector_assembler = VectorAssembler(
        inputCols=[x_col, y_col], outputCol="features")
    df_assembled = vector_assembler.transform(df)

    kmeans = KMeans(k=k, featuresCol="features", predictionCol="cluster")
    model = kmeans.fit(df_assembled)
    df_clustered = model.transform(df_assembled)

    scatter_fig = px.scatter(df_clustered.toPandas(), x=x_col, y=y_col, color="cluster", hover_data=[city_code_col],
                             title=f"K-means Clustering: {y_col} vs {x_col} (k={k})")

    return scatter_fig


def plot_linear_regression(df, x_col, y_col, city_code_col):
    """
    Perform linear regression on the provided DataFrame and plot the results.

    Parameters:
    - df: PySpark DataFrame
    - x_col: Name of the column for x-axis
    - y_col: Name of the column for y-axis
    - city_code_col: Name of the column for city codes (used in hover data)

    Returns:
    - Plotly scatter plot
    """
    # Vector Assembler
    vector_assembler = VectorAssembler(inputCols=[x_col], outputCol="features")
    df_assembled = vector_assembler.transform(df)

    # Linear Regression
    lr = LinearRegression(featuresCol="features", labelCol=y_col)
    model = lr.fit(df_assembled)

    # Make predictions
    df_predicted = model.transform(df_assembled)

    # Calculate residuals based on smallest perpendicular distance
    df_residuals = df_predicted.withColumn("residuals",
                                           col(y_col) - (col(x_col) * model.coefficients[0] + model.intercept))
    # Calculate ratio niveau_vie/loyer
    ratio_col_name = f"ratio_{y_col}_over_{x_col}"
    df_residuals = df_residuals.withColumn(
        ratio_col_name, col(y_col) / col(x_col))

    all_residuals = (
        df_residuals
        .orderBy(col("residuals"), ascending=True)
        .select("INSEE_C", "city_code", "residuals", ratio_col_name, "Population", "latitude", "longitude")
    )

    # Plotting
    scatter_fig = px.scatter(df_residuals.toPandas(), x=x_col, y=y_col, trendline="ols", hover_data=[city_code_col],
                             color="residuals", title=f"Linear Regression: {y_col} vs {x_col}")

    return scatter_fig, all_residuals


def analyze_df_from_path(df_folder_name):
    basepath = "./dataset/"
    csv_folder_files = os.listdir(f"{basepath}{df_folder_name}")
    csv_file = [file for file in csv_folder_files if file.endswith(".csv")][0]
    csv_path = basepath + "/" + df_folder_name + "/" + csv_file
    df = spark.read.csv(csv_path, header=True, inferSchema=True, sep=",")

    # Save stats
    describe_rdd = df.describe().rdd
    describe_rdd.saveAsTextFile(basepath + f"/{df_folder_name}" + "_stats.txt")

    # Example: K-means clustering plot
    x_col = "niveau_de_vie_moyen"
    y_col = "loypredm2"
    kmeans_scatter_plot = plot_kmeans_clustering(
        df, x_col, y_col, city_code_col="city_code", k=3)
    kmeans_scatter_plot.show()

    # Example: Linear regression plot
    lr_scatter_plot, all_residuals = plot_linear_regression(
        df, x_col, y_col, city_code_col="city_code")
    lr_scatter_plot.update_layout(
        title_text=f"Linear Regression ({df_folder_name}): {y_col} vs {x_col}")
    lr_scatter_plot.show()

    # Saving Residuals
    all_residuals.coalesce(1).write.json(
        f"./dataset/residuals_{df_folder_name}_json", mode="overwrite")


if __name__ == "__main__":

    # Spark fixes
    os.environ["HADOOP_HOME"] = "/home/jules/TSP/CSC_Data/hadoop-3.3.6"
    hadoop_native_path = os.path.join(
        os.environ["HADOOP_HOME"], "lib", "native")
    os.environ["LD_LIBRARY_PATH"] = f"{hadoop_native_path}:{os.environ.get('LD_LIBRARY_PATH', '')}"

    # Set up Spark session
    spark = SparkSession.builder.appName("Analysis").getOrCreate()

    # Create analysis data
    analyze_df_from_path("merged_dataset_maisons_csv")
    analyze_df_from_path("merged_dataset_apparts_csv")
