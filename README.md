# CSC5003 - Project

# Data Exploration and Analysis

This is a school project in Big Data where we analyze the following dataset :
https://www.data.gouv.fr/fr/datasets/carte-des-loyers-indicateurs-de-loyers-dannonce-par-commune-en-2023/.

Recently, inflation has impacted the standards of living of the average french person. With that said, thanks to the boom of working from home, one does not necessarily have to live near the workplace anymore and that opens up many possibilities on the place where one decides to live.
Hence our business objective which is to find where in France it would be advantageous to live comfortably based on the dataset of french rent prices in 2023.

We will be using Spark on python for data cleaning and analysis.

## Team Members
- Malek HAMMOU (malek.hammou@telecom-sudparis.eu)
- Jules RISSE (jules.risse@telecom-sudparis.eu)
- Devan PRIGENT (devan.prigent@telecom-sudparis.eu)

## How To use

Requirements : Python > 3.8

Create and activate python virtual environment:  
`python3 -m venv venv-data`  
`source ./venv-data/bin/activate`

Then install the necessary packages from requirements:  
`pip install -r requirements.txt`
